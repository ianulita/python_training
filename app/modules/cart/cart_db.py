import bson

class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getItems(self, status, username):
        return self.conn.find({'status':status, 'user':username}).sort([('_id',-1)])

    def createItem(self, item_name, item_price, username):
        self.conn.insert({'name':item_name, 'price':item_price, 'user':username, 'status':"new"})

    def addToCart(self, item_id, item_name, item_price, quantity, total_price, username):
        x = self.conn.find({'name':item_name, 'price':item_price, 'user':username,'status':"in_cart"}).limit(1).count()
        if(x==1):
            item = self.conn.find_one({'name':item_name, 'price':item_price, 'user':username, 'status':"in_cart"})
            quantity = int(quantity) + int(item['quantity'])
            quantity = int(quantity)
            total_price = total_price + float(item['total_price'])
            self.conn.update({'name':item_name, 'price':item_price,  'user':username, 'status':"in_cart"}, {'name':item_name, 'price':item_price, 'quantity':quantity, 'user':username, 'total_price': total_price, 'status':"in_cart"})
        else:
            self.conn.insert({'name':item_name, 'price':item_price, 'quantity':quantity, 'total_price': total_price, 'user':username, 'status':"in_cart"})
    
    def purchase(self, item_id, name, quantity, username):
        self.conn.update({'_id':bson.ObjectId(oid=str(item_id))}, {'name':name, 'quantity':quantity, 'user':username, 'status':"purchased"})
    
    def getItem(self, pid, username):
        return self.conn.find_one({'_id':bson.ObjectId(oid=str(pid)), 'user':username})
    
    def editItem(self, pid, item_name, item_price, username, old_name, old_price):
        in_cart = self.conn.find({'name':old_name, 'price':old_price, 'user':username,'status':"in_cart"}).limit(1).count()
        if (in_cart==1):
            item = self.conn.find_one({'name':old_name, 'price':old_price, 'user':username,'status':"in_cart"})
            total = int(item['quantity']) * float(item_price)
            total = int(total)
            self.conn.update({'name':old_name, 'price':old_price,  'user':username, 'status':"in_cart"}, {'name':item_name, 'price':item_price, 'quantity':item['quantity'], 'user':username, 'total_price': total, 'status':"in_cart"})
        self.conn.update({'_id':bson.ObjectId(oid=str(pid))}, {'name':item_name, 'price':item_price, 'user':username, 'status':"new"})