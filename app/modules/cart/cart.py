from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import url_for
from flask import flash
from flask import redirect
from flask import session
from flask import jsonify

mod = Blueprint('cart', __name__)
@mod.route('/')
def cart_layout():
	return render_template('cart/cart.html')

@mod.route('/get_inventory_content', methods=['GET','POST'])
def inventory_content(items=None):
	if request.method == "POST":
		if request.form['item_price']:
			item_name = request.form['item_name']
			item_price = request.form['item_price']
			g.cartdb.createItem(item_name, item_price, session['username'])
		if request.form['item_id']:
			item_ids = request.form.getlist("item_id")
			names = request.form.getlist("name")
			prices = request.form.getlist("price")
			quantities = request.form.getlist("quantity")
			for index,name in enumerate(names):
				if quantities[index]:	
					if int(quantities[index])>0:
						total_price = float(prices[index])*int(quantities[index])
						g.cartdb.addToCart(item_ids[index], name, prices[index], quantities[index], total_price, session['username'])
		
	items = g.cartdb.getItems("new", session['username'])
	item_list = []
	# cart_items = g.cartdb.getItems("in_cart", session['username'])
	# purchased_items = g.cartdb.getItems("purchased", session['username'])
	# return render_template('cart/cart.html', items=items, cart_items=cart_items, purchased_items=purchased_items)	
	for item in items:
		item_list.append({'item_name':item['name'], 'item_price':item['price'], 'item_id':str(item['_id'])})
	return jsonify(item_list=item_list)


    

@mod.route('/add_to_cart', methods=['POST'])
def add_to_cart():
	flash('Items successfully added to cart!', 'add_to_cart_success')
	return redirect(url_for('.cart_layout'))

@mod.route('/purchase', methods=['POST'])
def purchase():
	item_ids = request.form.getlist("item_id")
	names = request.form.getlist("name")
	quantities = request.form.getlist("quantity")
	for index,item_id in enumerate(item_ids):
		g.cartdb.purchase(item_id, names[index], quantities[index], session['username'])
	flash('Items successfully purchased!', 'add_to_cart_success')
	return redirect(url_for('.cart_layout'))	


@mod.route('/edit/<item_id>', methods=['GET','POST'])
def edit_item(item_id):
	if request.method == "POST":
		item_name = request.form['item_name']
		item_price = request.form['item_price']
		old_name = request.form['old_name']
		old_price = request.form['old_price']
		g.cartdb.editItem(item_id, item_name, item_price, session['username'], old_name, old_price)
		flash('Item Details Changed!', 'create_item_success')
		return redirect(url_for('.cart_layout'))
	else:
		item = g.cartdb.getItem(item_id, session['username'])
		return render_template('cart/edit.html', item=item)