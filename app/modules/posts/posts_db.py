import bson

class PostDB:
    def __init__(self, conn):
        self.conn = conn

    def getPosts(self, username):
        return self.conn.find({'username': username})
    def createPost(self, post, emotion, username):
        self.conn.insert({'post':post, 'emotion':emotion, 'username': username})
    def getPost(self, pid):
    	return self.conn.find_one({'_id':bson.ObjectId(oid=str(pid))})
    def editPost(self, pid, post, emotion, username):
    	self.conn.update({'_id':bson.ObjectId(oid=str(pid))}, {'post':post, 'emotion':emotion, 'username':username})