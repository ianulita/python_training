from flask import Blueprint
from flask import render_template
from flask import g
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask import session

mod = Blueprint('posts', __name__)

@mod.route('/')
def post_list():
    posts = g.postsdb.getPosts(session['username'])
    return render_template('posts/post.html', posts=posts, date='October 20 2015')

@mod.route('/', methods=['POST'])
def create_post():
    new_post = request.form['new_post']
    emotion = request.form['emotion']
    g.postsdb.createPost(new_post, emotion, session['username'])
    flash('New post created!', 'create_post_success')
    return redirect(url_for('.post_list'))

@mod.route('/edit/<pid>', methods=['GET', 'POST'])
def edit_post(pid):
	print pid
	if request.method == "POST":
		new_post = request.form['new_post']
		emotion = request.form['emotion']
		g.postsdb.editPost(pid, new_post, emotion, session['username'])
		flash('Form Edited Successfully!', 'create_post_success')
		return redirect(url_for('.post_list'))
	else:
		post = g.postsdb.getPost(pid)
		return render_template('posts/edit.html', post=post)
	