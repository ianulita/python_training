var DefaultURLMixin = {
  getDefaultProps: function () {
      return {url: "get_inventory_content", pollInterval: 1000};
  },
  propTypes: {
    pollInterval: React.PropTypes.number
  }
};


var Item = React.createClass({
  propTypes: {
    item: React.PropTypes.shape({
      item_name: React.PropTypes.string,
      item_price: React.PropTypes.any
    }),
  },
  render: function() {
    return (
      <tr>
      <td>{this.props.item.item_name}</td>
      <td>{this.props.item.item_price}</td>
      <td>
        <input type="text" placeholder="0.00" name="quantity"/>
        <input type="hidden" name="item_name" value={this.props.item.item_name}/>
        <input type="hidden" name="item_id" value={this.props.item.item_id}/>
      </td>
      </tr>
    );
  }
});


var ItemList = React.createClass({
  render: function() {
    var itemNodes = this.props.data.map(function (item, index) {
      return (
        <Item key={index} item={item} />
      );
    });
    var style = {
      width: "100%"
    }
    return (
    <table style={style}>
      <th>Item Name</th><th>Price</th><th>Quantity</th>
      {itemNodes}
    </table>
    );
  }
});



var InventoryBox = React.createClass({
  mixins: [DefaultURLMixin],
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var quantity = form.quantity.value.trim();
    if (!quantity) {
      return;
    }
    this.props.onCommentSubmit({item_name: item_name, item_price: item_price, quantity:quantity});
    form.item_name.value = '';
    form.item_price.value = '';
    return;
  },
  getInitialState: function() {
    return {data: []};
  },
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var itemDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: itemDict['item_list']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['item_list']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
    <form onSubmit={this.handleSubmit}>
      <ItemList data={this.state.data} />
      <br />
      <input type="submit" value="Add to Cart" />
    </form>
    );
  }
});








var PostForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var item_name = form.item_name.value.trim();
    var item_price = form.item_price.value.trim();
    if (!item_name || !item_price) {
      return;
    }
    this.props.onCommentSubmit({item_name: item_name, item_price: item_price});
    form.item_name.value = '';
    form.item_price.value = '';
    return;
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Item Name" name="item_name"/>
        <input type="text" placeholder="0.00" name="item_price"/>
        <input type="submit" value="Add Item" />
      </form>
    );
  }
});







var ItemForm = React.createClass({
  mixins: [DefaultURLMixin],
  getInitialState: function() {
    return {data: []};
  },
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var itemDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: itemDict['item_list']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['item_list']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
    <PostForm onCommentSubmit={this.handleCommentSubmit} />
    );
  }
});